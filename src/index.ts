#!/usr/bin/env node

import chalk from "chalk"
import {program} from "commander"
import * as console from "console"
import {requireModule} from "./Helpers.js"
import {spinnerSuccess, updateSpinnerText} from "./Spinner.js"
import figlet from "figlet"

const packageJson : {version: string, bin: string, description: string} = requireModule("../package.json")

console.log(chalk.yellow.bold(figlet.textSync("Multi-cli", {font: "Small Slant"})))

console.log(`Version ${packageJson.version}`)

program
    .name(Object.keys(packageJson.bin)[0])
    .description(packageJson.description)
    .version(packageJson.version)

program.command("status")
    .summary("Do something")
    .description("Just show that the cli is working")
    .action(async () => {
        updateSpinnerText("Doing something")
        await new Promise(resolve => setTimeout(resolve, 1000))
        spinnerSuccess("Done")
    })


program.parse()
