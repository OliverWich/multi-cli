import ora from "ora"


const spinner = ora({ // make a singleton, so we don't ever have 2 spinners
    // Damn you typescript, this type is here
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    spinner: "sand",
    color: "yellow"
})

export const updateSpinnerText = (message: string) => {
    if(spinner.isSpinning) {
        spinner.text = message
        return
    }
    spinner.start(message)
}

export const spinnerError = (message?: string) => {
    if(spinner.isSpinning) {
        spinner.fail(message)
    }
}
export const spinnerSuccess = (message?: string) => {
    if(spinner.isSpinning) {
        spinner.succeed(message)
    }
}

export const logInfo = (message?: string | Array<unknown> | object) => {
    spinner.clear()
    spinner.frame()

    console.log(message)
}
