import {createRequire} from "node:module"
import {resolve} from "node:path"
import { dirname } from 'path';
import { fileURLToPath } from 'url';

export const requireModule = createRequire(import.meta.url)

export const relativePathToAbsolute = (relativePath: string) => {
    return resolve(relativePath)
}

export const __dirname = dirname(fileURLToPath(import.meta.url)).slice(0, -4)
